package com.example.siva1.studzone;

/**
 * Created by siva1 on 19/12/2017.
 */

public class Credential
{
    String rollno;
    String password;

    public Credential(String rollno, String password) {
        this.rollno = rollno;
        this.password = password;
    }

    public String getRollno() {
        return rollno;
    }

    public void setRollno(String rollno) {
        this.rollno = rollno;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
