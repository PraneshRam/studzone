package com.example.siva1.studzone;

import com.google.gson.annotations.SerializedName;

/**
 * Created by keerthisri on 14-Feb-18.
 */

public class LabCAEntry {
    @SerializedName("CA - 1") String ca_1;
    @SerializedName("CA - 2") String ca_2;
    @SerializedName("Final Exam") String fin_Exam;
    @SerializedName("Final Viva Exam") String labViva;
    @SerializedName("Total") String total;
    @SerializedName("COURSE CODE")String course;
    @SerializedName("Attendance")String attendance;
    @SerializedName("COURSE TITLE") String title;
    @SerializedName("Viva Voce / Software package development") String viva;
    @SerializedName("Record / Test I") String labtest_1;
    @SerializedName("Record / Test II") String labtest_2;
    @SerializedName("Lab examination") String lab_exam;

    public LabCAEntry(String ca_1, String ca_2, String fin_Exam, String labViva, String total, String course, String attendance, String title, String viva, String labtest_1, String labtest_2, String lab_exam) {
        this.ca_1 = ca_1;
        this.ca_2 = ca_2;
        this.fin_Exam = fin_Exam;
        this.labViva = labViva;
        this.total = total;
        this.course = course;
        this.attendance = attendance;
        this.title = title;
        this.viva = viva;
        this.labtest_1 = labtest_1;
        this.labtest_2 = labtest_2;
        this.lab_exam = lab_exam;
    }

    @Override
    public String toString() {
        return "LabCAEntry{" +
                "ca_1='" + ca_1 + '\'' +
                ", ca_2='" + ca_2 + '\'' +
                ", fin_Exam='" + fin_Exam + '\'' +
                ", labViva='" + labViva + '\'' +
                ", total='" + total + '\'' +
                ", course='" + course + '\'' +
                ", attendance='" + attendance + '\'' +
                ", title='" + title + '\'' +
                ", viva='" + viva + '\'' +
                ", labtest_1='" + labtest_1 + '\'' +
                ", labtest_2='" + labtest_2 + '\'' +
                ", lab_exam='" + lab_exam + '\'' +
                '}';
    }

    public String getCa_1() {
        return ca_1;
    }

    public void setCa_1(String ca_1) {
        this.ca_1 = ca_1;
    }

    public String getCa_2() {
        return ca_2;
    }

    public void setCa_2(String ca_2) {
        this.ca_2 = ca_2;
    }

    public String getFin_Exam() {
        return fin_Exam;
    }

    public void setFin_Exam(String fin_Exam) {
        this.fin_Exam = fin_Exam;
    }

    public String getLabViva() {
        return labViva;
    }

    public void setLabViva(String labViva) {
        this.labViva = labViva;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getViva() {
        return viva;
    }

    public void setViva(String viva) {
        this.viva = viva;
    }

    public String getLabtest_1() {
        return labtest_1;
    }

    public void setLabtest_1(String labtest_1) {
        this.labtest_1 = labtest_1;
    }

    public String getLabtest_2() {
        return labtest_2;
    }

    public void setLabtest_2(String labtest_2) {
        this.labtest_2 = labtest_2;
    }

    public String getLab_exam() {
        return lab_exam;
    }

    public void setLab_exam(String lab_exam) {
        this.lab_exam = lab_exam;
    }
}
