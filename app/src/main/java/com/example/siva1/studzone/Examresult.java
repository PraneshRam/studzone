package com.example.siva1.studzone;

import com.google.gson.annotations.SerializedName;

/**
 * Created by siva1 on 18/12/2017.
 */

public class Examresult {
    @SerializedName("Course") String Course;
    @SerializedName("Title") String Title;
    @SerializedName("Mark") String Mark;
    @SerializedName("Result") String Result;

    public Examresult() {
    }

    public Examresult(String course, String title, String mark, String result) {
        Course = course;
        Title = title;
        Mark = mark;
        Result = result;
    }

    public String getCourse() {
        return Course;
    }

    public void setCourse(String course) {
        Course = course;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMark() {
        return Mark;
    }

    public void setMark(String mark) {
        Mark = mark;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    @Override
    public String toString() {
        return "Examresult{" +
                "Course='" + Course + '\'' +
                ", Title='" + Title + '\'' +
                ", Mark='" + Mark + '\'' +
                ", Result='" + Result + '\'' +
                '}';
    }
}
