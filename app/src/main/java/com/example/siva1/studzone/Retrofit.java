package com.example.siva1.studzone;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by siva1 on 18/12/2017.
 */

public interface Retrofit {
@FormUrlEncoded
    @POST("/ExamResults")
    void getExamresults(@Field("rollno") String roll,@Field("password") String password, Callback<Response> responseCallback);
    @FormUrlEncoded
    @POST("/CAMarks")
    void getCAresults(@Field("rollno") String roll,@Field("password") String password, Callback<Response> responseCallback);
    @FormUrlEncoded
    @POST("/Attendance")
    void getAttendance(@Field("rollno") String roll,@Field("password") String password, Callback<Response> responseCallback);
}
