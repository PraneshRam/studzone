package com.example.siva1.studzone;

import com.google.gson.annotations.SerializedName;

/**
 * Created by siva1 on 22/12/2017.
 */

public class TheoryCAEntry {
    @SerializedName("COURSE CODE")String course;
    @SerializedName("Attendance")String attendance;
    @SerializedName("COURSE TITLE") String title;
    @SerializedName("Test 1") String test_1;
    @SerializedName("Test 2") String test_2;
    @SerializedName("Test 3") String test_3;

    @SerializedName("Total (Best 2 TESTS)") String bestoftwo;

    @SerializedName("Assignment / Assignment Presentation") String assgn_ppt;

    @SerializedName("Assmt Tut-I") String assign_1;
    @SerializedName("Assmt Tut-II") String assign_2;
    @SerializedName("Objective test1 / Mini project1") String assignment_1;
    @SerializedName("Objective test2 / Mini project2") String assignment_2;
    @SerializedName("Obj.test1 / Mini project1 Ass.1 / Ass.tutorial 1") String assgn_1;
    @SerializedName("Obj.test2 / Mini project2 Ass.2 / Ass.tutorial 2") String assgn_2;

    @SerializedName("Total") String total;
    @SerializedName("viva") String viva;
    @SerializedName("Record / Mini Project") String record;
    @SerializedName("Final Lab Test") String labTest;

    public TheoryCAEntry(String course, String attendance, String title, String test_1, String test_2, String test_3, String bestoftwo, String assgn_ppt, String assign_1, String assign_2, String assignment_1, String assignment_2, String assgn_1, String assgn_2, String total, String viva, String record, String labTest) {
        this.course = course;
        this.attendance = attendance;
        this.title = title;
        this.test_1 = test_1;
        this.test_2 = test_2;
        this.test_3 = test_3;
        this.bestoftwo = bestoftwo;
        this.assgn_ppt = assgn_ppt;
        this.assign_1 = assign_1;
        this.assign_2 = assign_2;
        this.assignment_1 = assignment_1;
        this.assignment_2 = assignment_2;
        this.assgn_1 = assgn_1;
        this.assgn_2 = assgn_2;
        this.total = total;
        this.viva = viva;
        this.record = record;
        this.labTest = labTest;
    }

    @Override
    public String toString() {
        return "TheoryCAEntry{" +
                "course='" + course + '\'' +
                ", attendance='" + attendance + '\'' +
                ", title='" + title + '\'' +
                ", test_1='" + test_1 + '\'' +
                ", test_2='" + test_2 + '\'' +
                ", test_3='" + test_3 + '\'' +
                ", bestoftwo='" + bestoftwo + '\'' +
                ", assgn_ppt='" + assgn_ppt + '\'' +
                ", assign_1='" + assign_1 + '\'' +
                ", assign_2='" + assign_2 + '\'' +
                ", assignment_1='" + assignment_1 + '\'' +
                ", assignment_2='" + assignment_2 + '\'' +
                ", assgn_1='" + assgn_1 + '\'' +
                ", assgn_2='" + assgn_2 + '\'' +
                ", total='" + total + '\'' +
                ", viva='" + viva + '\'' +
                ", record='" + record + '\'' +
                ", labTest='" + labTest + '\'' +
                '}';
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTest_1() {
        return test_1;
    }

    public void setTest_1(String test_1) {
        this.test_1 = test_1;
    }

    public String getTest_2() {
        return test_2;
    }

    public void setTest_2(String test_2) {
        this.test_2 = test_2;
    }

    public String getTest_3() {
        return test_3;
    }

    public void setTest_3(String test_3) {
        this.test_3 = test_3;
    }

    public String getBestoftwo() {
        return bestoftwo;
    }

    public void setBestoftwo(String bestoftwo) {
        this.bestoftwo = bestoftwo;
    }

    public String getAssgn_ppt() {
        return assgn_ppt;
    }

    public void setAssgn_ppt(String assgn_ppt) {
        this.assgn_ppt = assgn_ppt;
    }

    public String getAssign_1() {
        return assign_1;
    }

    public void setAssign_1(String assign_1) {
        this.assign_1 = assign_1;
    }

    public String getAssign_2() {
        return assign_2;
    }

    public void setAssign_2(String assign_2) {
        this.assign_2 = assign_2;
    }

    public String getAssignment_1() {
        return assignment_1;
    }

    public void setAssignment_1(String assignment_1) {
        this.assignment_1 = assignment_1;
    }

    public String getAssignment_2() {
        return assignment_2;
    }

    public void setAssignment_2(String assignment_2) {
        this.assignment_2 = assignment_2;
    }

    public String getAssgn_1() {
        return assgn_1;
    }

    public void setAssgn_1(String assgn_1) {
        this.assgn_1 = assgn_1;
    }

    public String getAssgn_2() {
        return assgn_2;
    }

    public void setAssgn_2(String assgn_2) {
        this.assgn_2 = assgn_2;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getViva() {
        return viva;
    }

    public void setViva(String viva) {
        this.viva = viva;
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public String getLabTest() {
        return labTest;
    }

    public void setLabTest(String labTest) {
        this.labTest = labTest;
    }
}
