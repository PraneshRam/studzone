package com.example.siva1.studzone;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by siva1 on 18/12/2017.
 */

public class Resultadapter extends ArrayAdapter<Examresult> {
    Context context;
    int layoutId;
    ArrayList<Examresult> results;
    public Resultadapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Examresult> objects) {
        super(context, resource, objects);
        this.context=context;
        this.layoutId=resource;
        this.results= (ArrayList<Examresult>) objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.examresultrow, parent, false);
        }
        System.out.println("adapter"+results);
        Examresult examresult = results.get(position);
        TextView courseCode = (TextView)convertView.findViewById(R.id.courseCode);
        TextView courseTitle = (TextView)convertView.findViewById(R.id.courseTitle);
        TextView result = (TextView)convertView.findViewById(R.id.examResult);
        TextView grade = (TextView)convertView.findViewById(R.id.examGrade);
        courseCode.setText(examresult.getCourse());
        courseTitle.setText(examresult.getTitle());
        if(examresult.getResult()!=null && examresult.getResult().equalsIgnoreCase("RA")){
            grade.setTextColor(ContextCompat.getColor(grade.getContext(),R.color.Red));
            result.setTextColor(ContextCompat.getColor(result.getContext(),R.color.Red));
        }else{
            grade.setTextColor(ContextCompat.getColor(grade.getContext(),R.color.Green));
            result.setTextColor(ContextCompat.getColor(result.getContext(),R.color.Green));
        }
        grade.setText(examresult.getMark());
        result.setText(examresult.getResult());
        return convertView;
    }
}
