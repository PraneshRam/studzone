package com.example.siva1.studzone;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.siva1.studzone.databinding.FragmentAttendanceBinding;
import com.example.siva1.studzone.databinding.LayoutAttendanceListBinding;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by siva1 on 18/12/2017.
 */

public class AttendanceFragment extends Fragment{
    String rollno;
    String password;
    private String BASE_URL="http://192.168.0.2:5000";
    RestAdapter adapter;
    Retrofit attendancecall;
    ProgressDialog dialog;
    FragmentAttendanceBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_attendance,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rollno=getArguments().getString("rollno");
        password=getArguments().getString("password");
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Fetching");
        dialog.show();
        adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();
        attendancecall=adapter.create(Retrofit.class);
        attendancecall.getAttendance(rollno, password, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String str=ExamresultFragment.convertToResponseString(response);
                System.out.println(str);
                Attendanceresult result=new Gson().fromJson(str,Attendanceresult.class);
                dialog.dismiss();
                for(Attendance attendance : result.result){
                    final LayoutAttendanceListBinding dropdownBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),R.layout.layout_attendance_list
                            ,binding.attendanceLayout,false);
                    dropdownBinding.exmpHours.setText("Exemption Hours:"+attendance.getExemptionhrs());
                    dropdownBinding.dropDownListView.setText(attendance.getCoursecode());
                    dropdownBinding.precForAttend.setText("Percentage of Attendance:"+attendance.getPercentattend());
                    dropdownBinding.percForExemp.setText("Percentage with Exemption:"+attendance.getPercentexemp());
                    dropdownBinding.totAbsent.setText("Total Absent:"+attendance.getTotalabsent());
                    dropdownBinding.totHours.setText("Total Hours:"+attendance.getTotalhrs());
                    dropdownBinding.totPresent.setText("Total Present:"+attendance.getTotalpresent());
                    dropdownBinding.dropDownListView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(dropdownBinding.detailsLayout.getVisibility() == View.GONE) {
                                dropdownBinding.detailsLayout.setVisibility(View.VISIBLE);
                                dropdownBinding.dropDownListView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_up,0);
                            }else{
                                dropdownBinding.dropDownListView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_down,0);
                                dropdownBinding.detailsLayout.setVisibility(View.GONE);
                            }
                        }
                    });
                    binding.attendanceLayout.addView(dropdownBinding.getRoot());
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}

