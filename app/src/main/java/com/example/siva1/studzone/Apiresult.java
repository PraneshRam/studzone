package com.example.siva1.studzone;

import com.google.gson.annotations.SerializedName;

/**
 * Created by keerthisri on 14-Feb-18.
 */

public class Apiresult {
    @SerializedName("result") CAResult result;

    public CAResult getResult() {
        return result;
    }

    public void setResult(CAResult result) {
        this.result = result;
    }

    public Apiresult(CAResult result) {
        this.result = result;
    }
}
