package com.example.siva1.studzone;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private String rollno;
    private String password;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment newFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_camarks:
                    newFragment=new CamarksFragment();
                    Bundle bundle1=new Bundle();
                    bundle1.putString("rollno",rollno);
                    bundle1.putString("password",password);
                    newFragment.setArguments(bundle1);
                    getFragmentManager().beginTransaction().replace(R.id.content,newFragment).commit();
                    return true;
                case R.id.navigation_examresult:
                    newFragment=new ExamresultFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("rollno",rollno);
                    bundle.putString("password",password);
                    newFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.content,newFragment).commit();
                    return true;
                case R.id.navigation_attendance:
                    newFragment=new AttendanceFragment();
                    Bundle bundle2=new Bundle();
                    bundle2.putString("rollno",rollno);
                    bundle2.putString("password",password);
                    newFragment.setArguments(bundle2);
                    getFragmentManager().beginTransaction().replace(R.id.content,newFragment).commit();
                    return true;

            }
            return false;
        }

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_camarks);
        rollno=getIntent().getStringExtra("rollno");
        password=getIntent().getStringExtra("password");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_logout,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
                StudzonePreferences preferences = StudzonePreferences.getInstance(MainActivity.this);
                preferences.saveString("userName","");
                preferences.saveString("password","");
                finish();
                return (true);
        }
        return super.onOptionsItemSelected(item);
    }

}
