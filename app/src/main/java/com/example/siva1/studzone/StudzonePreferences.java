package com.example.siva1.studzone;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by keerthisri on 08-Feb-18.
 */

public class StudzonePreferences {
    SharedPreferences sharedPreferences;
    private static final String PREF_NAME = "STUDZONE";
    private static StudzonePreferences instance ;

    public static StudzonePreferences getInstance(Context context) {
        if(instance==null){
         return new StudzonePreferences(context);
        }
        return instance;
    }
    private StudzonePreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
    }
    public void saveString(String key,String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor .putString(key, value);
        prefsEditor.commit();
    }

    public String getString(String key) {
        if (sharedPreferences!= null) {
            return sharedPreferences.getString(key, "");
        }
        return "";
    }
}
