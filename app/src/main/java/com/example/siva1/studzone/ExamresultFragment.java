package com.example.siva1.studzone;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.siva1.studzone.databinding.FragmentExamresultBinding;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by siva1 on 18/12/2017.
 */

public class ExamresultFragment extends Fragment {
    private List<Examresult> examresultArrayList;
    private String BASE_URL="http://192.168.0.2:5000";
    RestAdapter adapter;
    Retrofit examresultcall;
    String rollno,password;
    ProgressDialog dialog;
    FragmentExamresultBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_examresult,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rollno=getArguments().getString("rollno");
        password=getArguments().getString("password");
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Fetching");
        dialog.show();
        adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();
        System.out.println(rollno+",,"+password);
        examresultcall=adapter.create(Retrofit.class);
        examresultArrayList=new ArrayList<>();
        examresultcall.getExamresults(rollno,password, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String res=convertToResponseString(response);
                System.out.println(res);
                Result a = new Gson().fromJson(res, Result.class);
                System.out.println("after gson"+a);
                examresultArrayList=a.getResult();
                System.out.println("afttttttt"+examresultArrayList);

                Resultadapter resultadapter=new Resultadapter(ExamresultFragment.this.getActivity(),R.layout.examresultrow,examresultArrayList);
                binding.lvExamResult.setAdapter(resultadapter);
                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println(error.getMessage());
            }
        });
        System.out.println("xxx"+examresultArrayList);
    }

    static public  String convertToResponseString(Response response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(response.getBody().in()));
            String line;

            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String result = sb.toString();
        return result;
    }
}
