package com.example.siva1.studzone;

import com.google.gson.annotations.SerializedName;

/**
 * Created by siva1 on 30/12/2017.
 */

public class Attendance {
    @SerializedName("COURSE CODE") String coursecode;
    @SerializedName("TOTAL HOURS") String totalhrs;
    @SerializedName("TOTAL PRESENT") String totalpresent;
    @SerializedName("TOTAL ABSENT") String totalabsent;
    @SerializedName("PERCENTAGE WITH EXEMP") String percentexemp;
    @SerializedName("PERCENTAGE OF ATTENDANCE") String percentattend;
    @SerializedName("EXEMPTION HOURS") String exemptionhrs;

    public String getCoursecode() {
        return coursecode;
    }

    public void setCoursecode(String coursecode) {
        this.coursecode = coursecode;
    }

    public String getTotalhrs() {
        return totalhrs;
    }

    public void setTotalhrs(String totalhrs) {
        this.totalhrs = totalhrs;
    }

    public String getTotalpresent() {
        return totalpresent;
    }

    public void setTotalpresent(String totalpresent) {
        this.totalpresent = totalpresent;
    }

    public String getTotalabsent() {
        return totalabsent;
    }

    public void setTotalabsent(String totalabsent) {
        this.totalabsent = totalabsent;
    }

    public String getPercentexemp() {
        return percentexemp;
    }

    public void setPercentexemp(String percentexemp) {
        this.percentexemp = percentexemp;
    }

    public String getPercentattend() {
        return percentattend;
    }

    public void setPercentattend(String percentattend) {
        this.percentattend = percentattend;
    }

    public String getExemptionhrs() {
        return exemptionhrs;
    }

    public void setExemptionhrs(String exemptionhrs) {
        this.exemptionhrs = exemptionhrs;
    }
}
