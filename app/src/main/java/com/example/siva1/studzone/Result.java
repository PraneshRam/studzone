package com.example.siva1.studzone;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by siva1 on 19/12/2017.
 */

public class Result {
    @SerializedName("result") List<Examresult> result;
    @SerializedName("name") String name;
    @SerializedName("rollno") String rollno;
    @SerializedName("programme") String programme;

    public Result() {
    }

    public Result(List<Examresult> result, String name, String rollno, String programme) {
        this.result = result;
        this.name = name;
        this.rollno = rollno;
        this.programme = programme;
    }

    public List<Examresult> getResult() {
        return result;
    }

    public void setResult(List<Examresult> result) {
        this.result = result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRollno() {
        return rollno;
    }

    public void setRollno(String rollno) {
        this.rollno = rollno;
    }

    public String getProgramme() {
        return programme;
    }

    public void setProgramme(String programme) {
        this.programme = programme;
    }

    @Override
    public String toString() {
        return "Result{" +
                "result=" + result +
                ", name='" + name + '\'' +
                ", rollno='" + rollno + '\'' +
                ", programme='" + programme + '\'' +
                '}';
    }
}
