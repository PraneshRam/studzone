package com.example.siva1.studzone;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.siva1.studzone.databinding.ActivityLoginBinding;

/**
 * Created by siva1 on 19/12/2017.
 */

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding binding;
    String userName,password;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login);
        checkSession();
        binding.showPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    binding.showPassword.setText("Hide Password");
                    binding.password.setInputType(InputType.TYPE_CLASS_TEXT);
                    binding.password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    binding.showPassword.setText("Show Password");
                    binding.password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    binding.password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
        binding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.username.getText().toString()!=null && binding.password.getText().toString()!=null){
                    userName = binding.username.getText().toString();
                    password = binding.password.getText().toString();
                    StudzonePreferences preferences = StudzonePreferences.getInstance(LoginActivity.this);
                    preferences.saveString("userName",userName);
                    preferences.saveString("password",password);
                    loadMainChild();
                }else{
                    Toast.makeText(LoginActivity.this, "Please enter the credential details...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void checkSession(){
        StudzonePreferences preferences = StudzonePreferences.getInstance(LoginActivity.this);
        if(preferences.getString("username")!="" && preferences.getString("password")!=""){
           loadMainChild();
        }else{
            binding.loginLayout.setVisibility(View.VISIBLE);
        }
    }
    private void loadMainChild(){
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        intent.putExtra("rollno",userName);
        intent.putExtra("password",password);
        startActivity(intent);
        finish();
    }

}
