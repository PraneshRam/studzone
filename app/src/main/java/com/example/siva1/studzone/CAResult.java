package com.example.siva1.studzone;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by siva1 on 22/12/2017.
 */

public class CAResult {
    @SerializedName("midresult") List<TheoryCAEntry> midResult;
    @SerializedName("labresult") List<LabCAEntry> labResult;
    @SerializedName("theoryresult") List<TheoryCAEntry> theoryResult;

    public CAResult(List<TheoryCAEntry> midResult, List<LabCAEntry> labResult, List<TheoryCAEntry> theoryResult) {
        this.midResult = midResult;
        this.labResult = labResult;
        this.theoryResult = theoryResult;
    }

    public List<TheoryCAEntry> getMidResult() {
        return midResult;
    }

    public void setMidResult(List<TheoryCAEntry> midResult) {
        this.midResult = midResult;
    }

    public List<LabCAEntry> getLabResult() {
        return labResult;
    }

    public void setLabResult(List<LabCAEntry> labResult) {
        this.labResult = labResult;
    }

    public List<TheoryCAEntry> getTheoryResult() {
        return theoryResult;
    }

    public void setTheoryResult(List<TheoryCAEntry> theoryResult) {
        this.theoryResult = theoryResult;
    }

    @Override
    public String toString() {
        return "CAResult{" +
                "midResult=" + midResult +
                ", labResult=" + labResult +
                ", theoryResult=" + theoryResult +
                '}';
    }
}
