package com.example.siva1.studzone;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.siva1.studzone.databinding.FragmentCamarksBinding;
import com.example.siva1.studzone.databinding.LayoutCamarkDropdownBinding;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.view.View.GONE;

/**
 * Created by siva1 on 17/12/2017.
 */

public class CamarksFragment extends Fragment {
    String rollno,password;
    LinearLayout genericLoader;
    private String BASE_URL="http://192.168.0.2:5000";
    RestAdapter adapter;
    FragmentCamarksBinding binding;
    Retrofit caresultcall;
    ProgressDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_camarks,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rollno=getArguments().getString("rollno");
        password=getArguments().getString("password");
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Fetching");
        dialog.show();
        adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();
        System.out.println(rollno+",,"+password);
        caresultcall=adapter.create(Retrofit.class);
        caresultcall.getCAresults(rollno, password, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String res=ExamresultFragment.convertToResponseString(response);
                Apiresult result=new Gson().fromJson(res,Apiresult.class);
                System.out.println(result);
                if(result.getResult().getMidResult()!=null) {
                    for (TheoryCAEntry caEntry : result.getResult().getMidResult()) {
                        final LayoutCamarkDropdownBinding dropdownBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),R.layout.layout_camark_dropdown,binding.marksLayout,false);
                        dropdownBinding.dropDownListView.setText(caEntry.course+" "+caEntry.title);
                        dropdownBinding.test3.setVisibility(View.VISIBLE);
                        dropdownBinding.assign1.setVisibility(View.VISIBLE);
                        dropdownBinding.assign2.setVisibility(View.VISIBLE);
                        dropdownBinding.presentation.setVisibility(View.VISIBLE);
                        dropdownBinding.totalOfTwo.setVisibility(View.VISIBLE);

                        dropdownBinding.test1.setText("Test 1 : "+caEntry.test_1);
                        dropdownBinding.test2.setText("Test 2 : "+caEntry.test_2);
                        dropdownBinding.test3.setText("Test 3 : "+caEntry.test_3);
                        if(caEntry.attendance!=null){
                            dropdownBinding.attendance.setVisibility(View.VISIBLE);
                            dropdownBinding.attendance.setText("Attendance :"+caEntry.attendance);
                        }else{
                            dropdownBinding.attendance.setVisibility(GONE);
                        }
                        if(caEntry.assign_1!=null && caEntry.assign_2!=null) {
                            dropdownBinding.assign1.setText("Assignment 1 : " + caEntry.assign_1);
                            dropdownBinding.assign2.setText("Assignment 2 : " + caEntry.assign_2);
                        }else if(caEntry.assgn_1!=null && caEntry.assgn_2!=null){
                            dropdownBinding.assign1.setText("Assignment 1 : " + caEntry.assgn_1);
                            dropdownBinding.assign2.setText("Assignment 2 : " + caEntry.assgn_2);
                        }else if(caEntry.assignment_2!=null){
                            dropdownBinding.assign1.setText("Assignment 1 : " + caEntry.assignment_1);
                            dropdownBinding.assign2.setText("Assignment 2 : " + caEntry.assignment_2);
                        }else{
                            dropdownBinding.assign2.setVisibility(GONE);
                            dropdownBinding.assign1.setVisibility(GONE);
                        }
                        if(caEntry.viva!=null){
                            dropdownBinding.viva.setText("Viva : "+caEntry.viva);
                            dropdownBinding.viva.setVisibility(View.VISIBLE);
                        }else{
                            dropdownBinding.viva.setVisibility(GONE);
                        }
                        dropdownBinding.totalOfTwo.setText("Total (Best Of 2) : "+caEntry.bestoftwo);
                        if(caEntry.assgn_ppt!=null) {
                            dropdownBinding.presentation.setText("Assignment Presentation : " + caEntry.assgn_ppt);
                        }else{
                            dropdownBinding.presentation.setVisibility(GONE);
                        }
                        if(caEntry.record!=null){
                            dropdownBinding.miniProject.setVisibility(View.VISIBLE);
                            dropdownBinding.labExam.setVisibility(View.VISIBLE);
                            dropdownBinding.miniProject.setText("Mini Project : "+caEntry.record);
                            dropdownBinding.labExam.setText("Final Lab Test :"+caEntry.labTest);
                        }else{
                            dropdownBinding.miniProject.setVisibility(View.GONE);
                            dropdownBinding.labExam.setVisibility(View.GONE);
                        }
                        dropdownBinding.total.setText("Total : "+caEntry.total);
                        dropdownBinding.dropDownListView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(dropdownBinding.detailsLayout.getVisibility() == View.GONE) {
                                    dropdownBinding.dropDownListView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_up,0);
                                    dropdownBinding.detailsLayout.setVisibility(View.VISIBLE);
                                }else{
                                    dropdownBinding.dropDownListView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_down,0);
                                    dropdownBinding.detailsLayout.setVisibility(View.GONE);
                                }
                            }
                        });
                        binding.marksLayout.addView(dropdownBinding.getRoot());
                    }
                }
                for(TheoryCAEntry caEntry : result.getResult().getTheoryResult()){
                    final LayoutCamarkDropdownBinding dropdownBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),R.layout.layout_camark_dropdown,binding.marksLayout,false);
                    dropdownBinding.dropDownListView.setText(caEntry.course+" "+caEntry.title);
                    dropdownBinding.test3.setVisibility(View.VISIBLE);
                    dropdownBinding.assign1.setVisibility(View.VISIBLE);
                    dropdownBinding.assign2.setVisibility(View.VISIBLE);
                    dropdownBinding.presentation.setVisibility(View.VISIBLE);
                    dropdownBinding.totalOfTwo.setVisibility(View.VISIBLE);

                    dropdownBinding.test1.setText("Test 1 : "+caEntry.test_1);
                    dropdownBinding.test2.setText("Test 2 : "+caEntry.test_2);
                    dropdownBinding.test3.setText("Test 3 : "+caEntry.test_3);
                    if(caEntry.attendance!=null){
                        dropdownBinding.attendance.setVisibility(View.VISIBLE);
                        dropdownBinding.attendance.setText("Attendance :"+caEntry.attendance);
                    }else{
                        dropdownBinding.attendance.setVisibility(GONE);
                    }
                    if(caEntry.assign_1!=null && caEntry.assign_2!=null) {
                        dropdownBinding.assign1.setText("Assignment 1 : " + caEntry.assign_1);
                        dropdownBinding.assign2.setText("Assignment 2 : " + caEntry.assign_2);
                    }else if(caEntry.assgn_1!=null && caEntry.assgn_2!=null){
                        dropdownBinding.assign1.setText("Assignment 1 : " + caEntry.assgn_1);
                        dropdownBinding.assign2.setText("Assignment 2 : " + caEntry.assgn_2);
                    }else if(caEntry.assignment_2!=null){
                        dropdownBinding.assign1.setText("Assignment 1 : " + caEntry.assignment_1);
                        dropdownBinding.assign2.setText("Assignment 2 : " + caEntry.assignment_2);
                    }else{
                        dropdownBinding.assign2.setVisibility(GONE);
                        dropdownBinding.assign1.setVisibility(GONE);
                    }
                    if(caEntry.viva!=null){
                        dropdownBinding.viva.setText("Viva : "+caEntry.viva);
                        dropdownBinding.viva.setVisibility(View.VISIBLE);
                    }else{
                        dropdownBinding.viva.setVisibility(GONE);
                    }
                    dropdownBinding.totalOfTwo.setText("Total (Best Of 2) : "+caEntry.bestoftwo);
                    if(caEntry.assgn_ppt!=null) {
                        dropdownBinding.presentation.setText("Assignment Presentation : " + caEntry.assgn_ppt);
                    }else{
                        dropdownBinding.presentation.setVisibility(GONE);
                    }
                    if(caEntry.record!=null){
                        dropdownBinding.miniProject.setVisibility(View.VISIBLE);
                        dropdownBinding.labExam.setVisibility(View.VISIBLE);
                        dropdownBinding.miniProject.setText("Mini Project : "+caEntry.record);
                        dropdownBinding.labExam.setText("Final Lab Test :"+caEntry.labTest);
                    }else{
                        dropdownBinding.miniProject.setVisibility(View.GONE);
                        dropdownBinding.labExam.setVisibility(View.GONE);
                    }
                    dropdownBinding.total.setText("Total : "+caEntry.total);
                    dropdownBinding.dropDownListView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(dropdownBinding.detailsLayout.getVisibility() == View.GONE) {
                                dropdownBinding.dropDownListView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_up,0);
                                dropdownBinding.detailsLayout.setVisibility(View.VISIBLE);
                            }else{
                                dropdownBinding.dropDownListView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_down,0);
                                dropdownBinding.detailsLayout.setVisibility(View.GONE);
                            }
                        }
                    });
                    binding.marksLayout.addView(dropdownBinding.getRoot());
                }
                for(LabCAEntry caEntry : result.getResult().getLabResult()){
                    final LayoutCamarkDropdownBinding dropdownBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),R.layout.layout_camark_dropdown,binding.marksLayout,false);
                    dropdownBinding.dropDownListView.setText(caEntry.course+" "+caEntry.title);
                    dropdownBinding.test3.setVisibility(GONE);
                    dropdownBinding.assign1.setVisibility(GONE);
                    dropdownBinding.assign2.setVisibility(GONE);
                    dropdownBinding.presentation.setVisibility(GONE);
                    if(caEntry.labtest_1!=null && caEntry.labtest_2!=null){
                        dropdownBinding.test1.setText("Test 1 : "+caEntry.labtest_1);
                        dropdownBinding.test2.setText("Test 2 : "+caEntry.labtest_2);
                    }else{
                        dropdownBinding.test1.setText("Test 1 : "+caEntry.ca_1);
                        dropdownBinding.test2.setText("Test 2 : "+caEntry.ca_2);
                    }
                    if(caEntry.attendance!=null){
                        dropdownBinding.attendance.setVisibility(View.VISIBLE);
                        dropdownBinding.attendance.setText("Attendance :"+caEntry.attendance);
                    }else{
                        dropdownBinding.attendance.setVisibility(GONE);
                    }
                    if(caEntry.viva!=null){
                        dropdownBinding.viva.setText("Viva : "+caEntry.viva);
                        dropdownBinding.viva.setVisibility(View.VISIBLE);
                    }else{
                        dropdownBinding.viva.setVisibility(GONE);
                    }
                    dropdownBinding.totalOfTwo.setVisibility(View.VISIBLE);
                    if(caEntry.fin_Exam!=null){
                        dropdownBinding.totalOfTwo.setText("Final Exam : "+caEntry.fin_Exam);
                    }else if(caEntry.lab_exam!=null){
                        dropdownBinding.totalOfTwo.setText("Final Exam : "+caEntry.lab_exam);
                    }else{
                        dropdownBinding.totalOfTwo.setVisibility(GONE);
                    }
                    dropdownBinding.total.setText("Total : "+caEntry.total);
                    dropdownBinding.dropDownListView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(dropdownBinding.detailsLayout.getVisibility() == View.GONE) {
                                dropdownBinding.dropDownListView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_up,0);
                                dropdownBinding.detailsLayout.setVisibility(View.VISIBLE);
                            }else{
                                dropdownBinding.dropDownListView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_down,0);
                                dropdownBinding.detailsLayout.setVisibility(View.GONE);
                            }
                        }
                    });
                    binding.marksLayout.addView(dropdownBinding.getRoot());
                }
                dialog.dismiss();
                System.out.println(result);
            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println(error.getUrl());
                System.out.println(error.getMessage());
            }
        });
    }
    public void enableOrDisableViews(String labExam){
        if(labExam!=null){

        }
    }
}
