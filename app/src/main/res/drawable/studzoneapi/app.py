import requests
from flask import Flask, request
from sample import getCAMarks,getExamResult,getStudentAttendance
from bs4 import BeautifulSoup

app = Flask(__name__)


@app.route('/', methods=['GET'])
def verify():
    print("GET")
    return "GET"

@app.route('/CAMarks', methods=['POST'])
def CAMarks():
    print("POST")
    data = request.form
    return getCAMarks(data["rollno"],data["password"]),200,{'Content-Type': 'application/json'}

@app.route('/ExamResults', methods=['POST'])
def ExamResults():
    print("POST")
    data = request.form
    return getExamResult(data["rollno"],data["password"]),200,{'Content-Type': 'application/json'}

@app.route('/Attendance', methods=['POST'])
def Attendance():
    print("POST")
    data = request.form
    return getStudentAttendance(data["rollno"],data["password"]),200,{'Content-Type': 'application/json'}


if __name__ == '__main__':
    app.run(host='192.168.0.4',debug=True)