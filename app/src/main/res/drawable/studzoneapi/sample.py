import requests
import json
from bs4 import BeautifulSoup

def getCAMarks(rollno,password):
    orig_url = "https://ecampus.psgtech.ac.in/studzone/"
    CA_URL= "https://ecampus.psgtech.ac.in/studzone/CAMarks_View.aspx"
    ATTENDANCE_URL= ""
    param = {'__EVENTTARGET':'',
                             '__EVENTARGUMENT':'',
                             '__LASTFOCUS':'',
                             '__VIEWSTATE':'aL20VJkUETIQR5w+blWAoj1WvCH1hp1S5kc9RSREZ5GTBdHKXi4aPdSazMOObxdjISCDF0H2SFpR/O6x0YpCVPg2DT8rf370ZUAbG5TMGAwZk6J8J92/KZ7Lotb/I7wIUCVTrepK4OWAsC3yorLft1+bIECd+lLZxhkjDoLzYqZwd/+ImnBaZ+EDc1ml4wkCx+pptCz6+Vow41Zs45gyJYcnTd3Wo+vBs1pFHMrx2zU=',
                             '__EVENTVALIDATION':'PU+jvNrpdQDxjJykZCzJU/Zf6eXDgcUoqtaA5gR5lfZrGlR/c1+uqLZ41dNDV7RIuiI/LnMuEgvrwrI3bpWabjf+ANv4KM2si6mQQ2U4uWBahcvoJla5jk6sia2u9E7db0w9ORMM2l7ic2EKNo684NXtEE+dz8nXovU6oJckVYpB1ZpgOouMr3kzT2OB5v2xvrz9vawEE+YIoNW9H6fWMAhyM+9FarJBFWyKZWOjfVvfJIT4T3bsZnbvxDR8tPVgQYYF3ctUKNF4skSPKFfWLQ==',
                             'rdolst':'S',
                             'Txtstudid':str(rollno),
                             'TxtPasswd':str(password),
                             'btnlogin':'Login'}

    initialSession = requests.Session()
    result = initialSession.get(orig_url, params=param)
    cookieinfo = initialSession.cookies.get_dict()


    CA_result = requests.get(CA_URL,cookies=cookieinfo).text
    soup=BeautifulSoup(CA_result,"html.parser")

    tableIds = []
    tables = soup.findChildren('table')
    for tag in tables:
        if (str(tag.get('id')).find("8^"))!=-1:
            tableIds.append(tag.get('id'))

    noofTables = len(tableIds)
    subjectList1=[]
    subjectList2=[]
    subjectList3=[]
    
    INDEX_VALUE_DICT_TAB1 = {}
    INDEX_VALUE_DICT_TAB2 = {}
    INDEX_VALUE_DICT_TAB3 = {}
    count=0
    for num in range(0,noofTables):

        table = soup.find(id=tableIds[num])
        rows = table.findChildren(['tr'])
        rowCount=1;
        index=1;

        for row in rows:
            if (tableIds[num] == "8^1260"):
                CA_MARKS_DICT_TAB1 = {}

            if (tableIds[num] == "8^1390"):
                CA_MARKS_DICT_TAB2 = {}

            if (tableIds[num] == "8^1400"):
                CA_MARKS_DICT_TAB3 = {}
                
            cells = row.findChildren('td')
            index=1;

            for cell in cells:
                if rowCount==1:
                    if (tableIds[num] == "8^1260"):
                        INDEX_VALUE_DICT_TAB1[index]=str(cell.string)
                        CA_MARKS_DICT_TAB1[INDEX_VALUE_DICT_TAB1[index]]=''

                    if (tableIds[num] == "8^1390"):
                        INDEX_VALUE_DICT_TAB2[index]=str(cell.string)
                        CA_MARKS_DICT_TAB2[INDEX_VALUE_DICT_TAB2[index]]=''

                    if (tableIds[num] == "8^1400"):
                        INDEX_VALUE_DICT_TAB3[index]=str(cell.string)
                        CA_MARKS_DICT_TAB3[INDEX_VALUE_DICT_TAB3[index]]=''
                          
                if rowCount>=3:
                    if (tableIds[num] == "8^1260"):
                        CA_MARKS_DICT_TAB1[INDEX_VALUE_DICT_TAB1[index]]=str(cell.string)
                    if (tableIds[num] == "8^1390"):
                        CA_MARKS_DICT_TAB2[INDEX_VALUE_DICT_TAB2[index]]=str(cell.string)
                    if (tableIds[num] == "8^1400"):
                        CA_MARKS_DICT_TAB3[INDEX_VALUE_DICT_TAB3[index]]=str(cell.string)
                    
                index=index+1
                
            if rowCount>=3:
                if (tableIds[num] == "8^1260"):
                    subjectList1.append(CA_MARKS_DICT_TAB1)
                if (tableIds[num] == "8^1390"):
                    subjectList2.append(CA_MARKS_DICT_TAB2)
                if (tableIds[num] == "8^1400"):
                    subjectList3.append(CA_MARKS_DICT_TAB3)
            rowCount=rowCount+1

    Result = {}
    
    Result['labresult'] = subjectList1
    Result['midresult'] = subjectList2
    Result['theoryresult'] = subjectList3

    CAResult = {}
    
    CAResult['result'] = Result
    CAResult['rollno'] = rollno
    CAResult['password'] = password
    CAResultJSON = json.dumps(CAResult)
    return CAResultJSON

def getExamResult(rollno,password):
    examResult = {}
    orig_url = "https://ecampus.psgtech.ac.in/studzone/"
    EXAM_RESULT_URL= "https://ecampus.psgtech.ac.in/studzone/FrmEpsStudResult.aspx"
    param = {'__EVENTTARGET':'',
                             '__EVENTARGUMENT':'',
                             '__LASTFOCUS':'',
                             '__VIEWSTATE':'aL20VJkUETIQR5w+blWAoj1WvCH1hp1S5kc9RSREZ5GTBdHKXi4aPdSazMOObxdjISCDF0H2SFpR/O6x0YpCVPg2DT8rf370ZUAbG5TMGAwZk6J8J92/KZ7Lotb/I7wIUCVTrepK4OWAsC3yorLft1+bIECd+lLZxhkjDoLzYqZwd/+ImnBaZ+EDc1ml4wkCx+pptCz6+Vow41Zs45gyJYcnTd3Wo+vBs1pFHMrx2zU=',
                             '__EVENTVALIDATION':'PU+jvNrpdQDxjJykZCzJU/Zf6eXDgcUoqtaA5gR5lfZrGlR/c1+uqLZ41dNDV7RIuiI/LnMuEgvrwrI3bpWabjf+ANv4KM2si6mQQ2U4uWBahcvoJla5jk6sia2u9E7db0w9ORMM2l7ic2EKNo684NXtEE+dz8nXovU6oJckVYpB1ZpgOouMr3kzT2OB5v2xvrz9vawEE+YIoNW9H6fWMAhyM+9FarJBFWyKZWOjfVvfJIT4T3bsZnbvxDR8tPVgQYYF3ctUKNF4skSPKFfWLQ==',
                             'rdolst':'S',
                             'Txtstudid':str(rollno),
                             'TxtPasswd':str(password),
                             'btnlogin':'Login'}

    initialSession = requests.Session()
    result = initialSession.get(orig_url, params=param)
    cookieinfo = initialSession.cookies.get_dict()

    CA_result = requests.get(EXAM_RESULT_URL,cookies=cookieinfo).text
    soup=BeautifulSoup(CA_result,"html.parser")

    tableIds = []
    subjectList = []
    tables = soup.findChildren('table')

    for tag in tables:
        INDEX_VALUE_DICT = {}
        if (str(tag.get('id')).find("DgResult"))!=-1:
            table = soup.find(id="DgResult")
            rows = table.findChildren(['tr'])
            rowCount = 1
            index = 1
            
            for row in rows:
                EXAM_RESULT_DICT = {}
                index = 1
                cells = row.findChildren('td')
                for cell in cells:
                    if rowCount==1:
                        if(cell.string == "Sem/Trim"):
                            INDEX_VALUE_DICT[index]=str("Sem")
                            EXAM_RESULT_DICT[INDEX_VALUE_DICT[index]] = ''
                        else:    
                            INDEX_VALUE_DICT[index]=str(cell.string)
                            EXAM_RESULT_DICT[INDEX_VALUE_DICT[index]] = ''
                    if rowCount>=2:
                        EXAM_RESULT_DICT[INDEX_VALUE_DICT[index]]=str(cell.string)

                    index = index + 1

                if rowCount>=2:
                    subjectList.append(EXAM_RESULT_DICT)
                rowCount = rowCount + 1
        if (str(tag.get('id')).find("TABLE3"))!=-1:
            table = soup.find(id="TABLE3")
            rows = table.findChildren(['tr'])

            for row in rows:
                cells = row.findChildren('td')
                for cell in cells:
                    spans = cell.findChildren('span')
                    for span in spans:
                        if(str((span.get('id'))) == "Label5"):
                           examResult['name'] = str(span.string)
                           
                        if(str((span.get('id'))) == "Label8"):
                           examResult['rollno'] = str(span.string)

        if (str(tag.get('id')).find("table9"))!=-1:
            table = soup.find(id="table9")
            rows = table.findChildren(['tr'])

            for row in rows:
                cells = row.findChildren('td')
                for cell in cells:
                    spans = cell.findChildren('span')
                    for span in spans:
                        if(str((span.get('id'))) == "Label6"):
                           examResult['programme'] = str(span.string)
                        
    examResult['result'] = subjectList
    examResultJSON = json.dumps(examResult)
    return examResultJSON

def getStudentAttendance(rollno,password):
    attendanceResult = {}
    orig_url = "https://ecampus.psgtech.ac.in/studzone/"
    STUD_ATTENDANCE_URL= "https://ecampus.psgtech.ac.in/studzone/AttWfPercView.aspx"
    param = {'__EVENTTARGET':'',
                             '__EVENTARGUMENT':'',
                             '__LASTFOCUS':'',
                             '__VIEWSTATE':'aL20VJkUETIQR5w+blWAoj1WvCH1hp1S5kc9RSREZ5GTBdHKXi4aPdSazMOObxdjISCDF0H2SFpR/O6x0YpCVPg2DT8rf370ZUAbG5TMGAwZk6J8J92/KZ7Lotb/I7wIUCVTrepK4OWAsC3yorLft1+bIECd+lLZxhkjDoLzYqZwd/+ImnBaZ+EDc1ml4wkCx+pptCz6+Vow41Zs45gyJYcnTd3Wo+vBs1pFHMrx2zU=',
                             '__EVENTVALIDATION':'PU+jvNrpdQDxjJykZCzJU/Zf6eXDgcUoqtaA5gR5lfZrGlR/c1+uqLZ41dNDV7RIuiI/LnMuEgvrwrI3bpWabjf+ANv4KM2si6mQQ2U4uWBahcvoJla5jk6sia2u9E7db0w9ORMM2l7ic2EKNo684NXtEE+dz8nXovU6oJckVYpB1ZpgOouMr3kzT2OB5v2xvrz9vawEE+YIoNW9H6fWMAhyM+9FarJBFWyKZWOjfVvfJIT4T3bsZnbvxDR8tPVgQYYF3ctUKNF4skSPKFfWLQ==',
                             'rdolst':'S',
                             'Txtstudid':str(rollno),
                             'TxtPasswd':str(password),
                             'btnlogin':'Login'}

    initialSession = requests.Session()
    result = initialSession.get(orig_url, params=param)
    cookieinfo = initialSession.cookies.get_dict()

    attendance_result = requests.get(STUD_ATTENDANCE_URL,cookies=cookieinfo).text
    soup=BeautifulSoup(attendance_result,"html.parser")

    tableIds = []
    subjectList = []
    tables = soup.findChildren('table')
    for tag in tables:
        INDEX_VALUE_DICT = {}
        if (str(tag.get('id')).find("PDGcourpercView"))!=-1:
            table = soup.find(id="PDGcourpercView")
            rows = table.findChildren(['tr'])
            rowCount = 1
            index = 1

            for row in rows:
                STUD_ATTENDANCE_DICT = {}
                index = 1
                cells = row.findChildren('td')
                for cell in cells:
                    if rowCount==1:
                        INDEX_VALUE_DICT[index]=str(cell.string)
                        STUD_ATTENDANCE_DICT[INDEX_VALUE_DICT[index]] = ''
                    if rowCount>=2:
                        STUD_ATTENDANCE_DICT[INDEX_VALUE_DICT[index]]=str(cell.string)

                    index = index + 1

                if rowCount>=2:
                    subjectList.append(STUD_ATTENDANCE_DICT)
                rowCount = rowCount + 1

    attendanceResult['result'] = subjectList
    attendanceResultJSON = json.dumps(attendanceResult)
    return attendanceResultJSON
    
#subjectList=getCAMarks("17pt09","18may00")
#print(subjectList)
#subjectList=getExamResult("16pw11","29aug98")
#print(subjectList)

subjectList=getStudentAttendance("14pw14","09sep96")
print(subjectList)
